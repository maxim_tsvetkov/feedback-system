export enum Sentiment {
  negative = -1,
  neutral = 0,
  positive = 1,
}

export interface ReviewTheme {
  theme_id: number;
  sentiment: Sentiment;
}

export interface Review {
  id: number;
  created_at: string;
  comment: string;
  themes: ReviewTheme[];
}
