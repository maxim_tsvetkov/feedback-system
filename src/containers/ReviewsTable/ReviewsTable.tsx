import React from 'react';
import { Table as SemTable } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { AppState } from '../../store';
import { getReviewError, getReviews, isLoadingReviews } from '../../store/review/selectors';
import { getThemes } from '../../store/theme/selectors';
import { modifyDateString } from '../../utils/helpers';
import Table from '../../components/Table/Table';
import ReviewThemeList from '../../components/ReviewThemeList/ReviewThemeList';

const HEADERS = ['Created at', 'Comment', 'Themes'];

const mapStateToProps = (state: AppState) => ({
  listError: getReviewError(state),
  isLoadingReviews: isLoadingReviews(state),
  reviews: getReviews(state),
  allThemes: getThemes(state),
});

type ReviewsTableProps = ReturnType<typeof mapStateToProps>;

const ReviewsTable: React.FC<ReviewsTableProps> = ({ reviews, allThemes, isLoadingReviews, listError }) => {

  return (
    <Table headers={HEADERS} isLoading={isLoadingReviews} error={listError} >
      {reviews.map(({ id, created_at, comment, themes }) => (
        <SemTable.Row key={`review-${id}`}>
          <SemTable.Cell>
            {modifyDateString(created_at)}
          </SemTable.Cell>
          <SemTable.Cell>
            {comment}
          </SemTable.Cell>
          <SemTable.Cell>
            <ReviewThemeList themes={allThemes} itemReviewTheme={themes} />
          </SemTable.Cell>
        </SemTable.Row>
      ))}
      {reviews.length === 0 && (
        <SemTable.Row>
          <SemTable.Cell colSpan='3'>
            No reviews
          </SemTable.Cell>
        </SemTable.Row>
      )}
    </Table>
  );
};

export default connect(mapStateToProps, {})(ReviewsTable);
