import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Grid } from 'semantic-ui-react';
import Filter from '../../components/Filter/Filter';
import ReviewsTable from '../ReviewsTable';
import Pagination from '../../components/Pagination/Pagination';
import { AppDispatch, AppState } from '../../store';
import { getCurrentPage, getFilterParams } from '../../store/review/selectors';
import { getCurrentReviews, updateFilterPage, updateFilterTheme } from '../../store/review/thunks';
import { getThemeError, getThemeOptions, isLoadingThemes } from '../../store/theme/selectors';
import { getAllThemes } from '../../store/theme/thunks';

const mapStateToProps = (state: AppState) => ({
  filterError: getThemeError(state),
  isLoadingThemes: isLoadingThemes(state),
  themeOptions: getThemeOptions(state),
  filter: getFilterParams(state),
  currentPage: getCurrentPage(state),
});

const mapDispatchToProps = (dispatch: AppDispatch) =>
  bindActionCreators({ getCurrentReviews, getAllThemes, updateFilterPage, updateFilterTheme }, dispatch);

type FeedPageProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

const FeedPage: React.FC<FeedPageProps> = ({
  getCurrentReviews,
  getAllThemes,
  isLoadingThemes,
  themeOptions,
  filter,
  filterError,
  updateFilterPage,
  updateFilterTheme,
  currentPage,
}) => {
  useEffect(() => {
    getCurrentReviews();
    getAllThemes();
  }, []);

  const handleFilterChange = (value?: number) => {
    updateFilterTheme(value);
  };

  const handlePageChange = (page?: number) => {
    updateFilterPage(page);
  };

  return (
    <Grid data-test-id='feedPage'>
      <Grid.Column width={4}>
        <Filter
          options={themeOptions}
          currentItemId={filter?.theme_id}
          isLoading={isLoadingThemes}
          onChange={handleFilterChange}
          error={filterError}
        />
      </Grid.Column>
      <Grid.Column width={12}>
        <ReviewsTable />
        <Pagination currentPage={currentPage} onPageChange={handlePageChange} totalPage={3} />
      </Grid.Column>
    </Grid>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(FeedPage);
