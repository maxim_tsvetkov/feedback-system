import React, { memo } from 'react';
import { Pagination as SemanticPagination, PaginationProps as SmPaginationProps } from 'semantic-ui-react';

interface PaginationProps {
  onPageChange: (page?: number) => void;
  totalPage: number;
  currentPage?: number;
}

const Pagination: React.FC<PaginationProps> = ({ onPageChange, totalPage, currentPage = 0 }) => (
  <SemanticPagination
    firstItem={null}
    lastItem={null}
    pointing
    secondary
    totalPages={totalPage}
    activePage={currentPage}
    onPageChange={(_, { activePage }: SmPaginationProps) => {
      // NOTICE: the page numbering starts from 0 instead of 1
      onPageChange(typeof activePage === 'number' ? activePage - 1 : undefined);
    }}
  />
);

export default memo(Pagination);
