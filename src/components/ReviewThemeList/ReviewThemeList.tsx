import * as React from 'react';
import { List, SemanticCOLORS, SemanticICONS } from 'semantic-ui-react';
import { Theme } from '../../interfaces/theme.interface';
import { ReviewTheme, Sentiment } from '../../interfaces/review.interface';

interface ListProps {
  themes: Theme[];
  itemReviewTheme: ReviewTheme[];
}

type SentimentIconMap = {
  [key in Sentiment]: SemanticICONS;
};

type SentimentIconColorMap = {
  [key in Sentiment]: SemanticCOLORS;
};

const SentimentIcons: SentimentIconMap = {
  [Sentiment.negative]: 'thumbs down outline',
  [Sentiment.neutral]: 'file outline',
  [Sentiment.positive]: 'thumbs up outline',
};

const SentimentIconColors: SentimentIconColorMap = {
  [Sentiment.negative]: 'red',
  [Sentiment.neutral]: 'orange',
  [Sentiment.positive]: 'teal',
};

const NO_NAME_THEME = 'No name theme';

const ReviewThemeList: React.FC<ListProps> = ({ themes, itemReviewTheme }) => {
  const getThemeNameById = (id: number) => themes?.find((theme) => theme.id === id)?.name || NO_NAME_THEME;

  return (
    <List verticalAlign="middle">
      {itemReviewTheme.map(({ theme_id, sentiment }, idx) => (
        <List.Item key={idx}>
          <List.Icon color={SentimentIconColors[sentiment]} name={SentimentIcons[sentiment]} />
          <List.Content>{getThemeNameById(theme_id)}</List.Content>
        </List.Item>
      ))}
    </List>
  );
};

export default React.memo(ReviewThemeList);
