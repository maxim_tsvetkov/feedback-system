import * as React from 'react';
import * as Enzyme from 'enzyme';

const { shallow } = Enzyme;

import ReviewThemeList from './ReviewThemeList';

describe('ReviewThemeList component', () => {
  const themes = [
    {
      id: 6334,
      name: 'Accounts',
    },
    {
      id: 6335,
      name: 'Transactions',
    },
    {
      id: 6336,
      name: 'Managing Payees',
    },
  ];

  const reviewThemes = [
    {
      theme_id: 6334,
      sentiment: -1,
    },
    {
      theme_id: 6335,
      sentiment: 0,
    },
    {
      theme_id: 6336,
      sentiment: 1,
    },
  ];

  let list;

  it('should render component', () => {
    list = shallow(<ReviewThemeList itemReviewTheme={[]} themes={[]} />);
  });

  it('should render list', () => {
    list = shallow(<ReviewThemeList itemReviewTheme={reviewThemes} themes={themes} />);
    expect(list.getElements()).toMatchSnapshot('./__snapshots__/ReviewThemeList.test.js.snap');
  });

  it('should render list without themes properly', () => {
    list = shallow(<ReviewThemeList itemReviewTheme={reviewThemes} themes={[]} />);
    expect(list.getElements()).toMatchSnapshot('./__snapshots__/ListNoNameThemes.test.js.snap');
  });
});
