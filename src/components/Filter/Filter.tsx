import React, { memo } from 'react';
import { Dropdown, Message, Image, Segment, DropdownProps } from 'semantic-ui-react';

interface FilterOption {
  key: number;
  text: string;
  value: number;
}

interface FilterProps {
  options: FilterOption[];
  currentItemId?: number;
  onChange: (value?: number) => void;
  isLoading: boolean;
  error: string | null;
}

const Filter: React.FC<FilterProps> = ({ currentItemId = undefined, options = [], onChange, isLoading, error }) => {
  if (isLoading) {
    return <Segment loading style={{ minHeight: 80 }} />;
  }

  return (
    <>
      {error && <Message error header="Filter error" content={error} />}
      <Dropdown
        clearable
        placeholder="Select theme"
        options={options}
        selection
        value={currentItemId}
        onChange={(_, { value }: DropdownProps) => {
          onChange(typeof value === 'number' ? value : undefined);
        }}
      />
    </>
  );
};

export default memo(Filter);
