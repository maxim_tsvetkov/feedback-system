import { action } from 'typesafe-actions';
import { Theme } from '../../interfaces/theme.interface';

export const FETCH_THEMES = 'FETCH_THEMES';
export const FETCH_THEMES_SUCCESS = 'FETCH_THEMES_SUCCESS';
export const FETCH_THEMES_FAILURE = 'FETCH_THEMES_FAILURE';
export const GET_THEME = 'GET_THEME';
export const GET_THEME_SUCCESS = 'GET_THEME_SUCCESS';
export const GET_THEME_FAILURE = 'GET_THEME_FAILURE';

export const fetchThemes = () => action(FETCH_THEMES);
export const fetchThemesSuccess = (themes: Theme[]) => action(FETCH_THEMES_SUCCESS, themes);
export const fetchThemesFailure = (error: string) => action(FETCH_THEMES_FAILURE, error);
export const getTheme = (id: number) => action(GET_THEME, id);
export const getThemeSuccess = (theme: Theme) => action(GET_THEME_SUCCESS, theme);
export const getThemeFailure = (error: string) => action(GET_THEME_FAILURE, error);

export type ThemeAction =
  | ReturnType<typeof fetchThemes>
  | ReturnType<typeof fetchThemesSuccess>
  | ReturnType<typeof fetchThemesFailure>
  | ReturnType<typeof getTheme>
  | ReturnType<typeof getThemeSuccess>
  | ReturnType<typeof getThemeFailure>;
