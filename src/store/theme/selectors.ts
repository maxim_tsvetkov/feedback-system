import { AppState } from '../index';

export const getThemeError = (state: AppState) => state.theme.error;

export const getThemes = (state: AppState) => state.theme.themes;

export const getThemeOptions = (state: AppState) => (state.theme.themes || []).map((theme, idx) => ({
  key: idx,
  text: theme.name,
  value: theme.id,
}));

export const isLoadingThemes = (state: AppState) => state.theme.isLoadingThemes;
