import { ThunkResult } from '../index';
import { fetchThemes, fetchThemesFailure, fetchThemesSuccess } from './actions';
import { getThemes } from '../../api/theme.api';

export const getAllThemes = (): ThunkResult<Promise<void>> => async (dispatch) => {
  dispatch(fetchThemes());
  try {
    const themes = await getThemes();
    dispatch(fetchThemesSuccess(themes));
  } catch (error) {
    dispatch(fetchThemesFailure(error.response?.data.message || 'Error'));
  }
};
