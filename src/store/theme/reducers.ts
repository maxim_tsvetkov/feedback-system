import {
  FETCH_THEMES,
  FETCH_THEMES_FAILURE,
  FETCH_THEMES_SUCCESS,
  GET_THEME,
  GET_THEME_FAILURE,
  GET_THEME_SUCCESS,
  ThemeAction,
} from './actions';
import { Theme } from '../../interfaces/theme.interface';

export interface ThemeState {
  isLoadingThemes: boolean;
  isLoadingTheme: boolean;
  themes: Theme[];
  theme: Theme | null;
  error: string | null;
}

const initialState: ThemeState = {
  isLoadingThemes: false,
  isLoadingTheme: false,
  themes: [],
  theme: null,
  error: null,
};

const theme = (state = initialState, action: ThemeAction): ThemeState => {
  switch (action.type) {
    case FETCH_THEMES:
      return {
        ...state,
        error: null,
        isLoadingThemes: true,
      };
    case FETCH_THEMES_SUCCESS:
      return {
        ...state,
        themes: action.payload,
        isLoadingThemes: false,
      };
    case FETCH_THEMES_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingThemes: false,
      };
    case GET_THEME:
      return {
        ...state,
        isLoadingTheme: true,
      };
    case GET_THEME_SUCCESS:
      return {
        ...state,
        theme: action.payload,
        isLoadingTheme: false,
      };
    case GET_THEME_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingTheme: false,
      };
    default:
      return state;
  }
};

export default theme;
