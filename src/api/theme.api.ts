import { Theme } from '../interfaces/theme.interface';
import internalAPI from './internalAPI';
import { APIResponse } from '../interfaces/api.interface';

export const getThemes = async (): Promise<Theme[]> => {
  const { data } = await internalAPI.get<APIResponse<Theme[]>>('api/themes');
  return data.data;
};

export const getThemeById = async (id: number): Promise<Theme> => {
  const response = await internalAPI.get<Theme>(`api/themes/${id}`);
  return response.data;
};
