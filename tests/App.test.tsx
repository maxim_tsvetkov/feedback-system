import * as React from 'react';
import * as Enzyme from 'enzyme';
import App from '../src/App';
import AppRouter from '../src/containers/AppRouter/AppRouter';

describe('App component', () => {
  let app;

  it('should render App component', () => {
    app = Enzyme.shallow(<App />);
  });

  it('should contain an inner router component', () => {
    app = Enzyme.shallow(<App />);

    expect(app.contains(<AppRouter />)).toBe(true);
  });
});
