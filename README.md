# Customer feedback system

#### Demo

[Netlify link](https://quizzical-swanson-69ef58.netlify.app/)

#### Technical details

SPA with React/Redux/Thunk
Custom webpack setup. The Prettier is used as code formatter.
UI is based on Semantic Library.

- [React](https://github.com/facebook/react)
- [TypeScript](https://github.com/microsoft/TypeScript)
- [Semantic UI](https://react.semantic-ui.com/)
- [Webpack](https://webpack.js.org/)
- [Redux](https://react-redux.js.org/)
- [Prettier](https://prettier.io/),
- [Cypress](https://www.cypress.io)

#### What could be better(TBD)

- increase code coverage, current coverage is available after npm run test:e2e && npm run open:coverage
- add CI scripts

#### Current unit test coverage (7 tests)

| % Stmts | % Branch | % Funcs | % Lines |

|   48.38 |    15.5  |   10.17 |   61.98 |

#### Test types

First of all during implementation of this task instead of coverage I was focused on showing
that I am familiar with different approaches in testing.

- e2e tests - cypress
- Standard unit test - src/utils/helpers.test.ts
- Enzyme snapshot - src/components/ReviewThemeList/ReviewThemeList.test.tsx 

#### CLI Commands

``` bash
# First install dependencies:
npm install

# To run in hot module reloading mode:
npm start

# To create a production build:
npm run build-prod

# To create a development build:
npm run build-dev

## Testing

# run tests with cypress in browser mode
npm run cypress

# run tests with cypress in console mode
npm run test:e2e

# run unit tests
npm run test

# open a code coverage report
npm run open:coverage
```

