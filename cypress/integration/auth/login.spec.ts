import { FeedPage, LoginPage } from '../../pages';

context('When user is on login page', () => {
  let page: LoginPage;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  beforeEach(() => {
    page = new LoginPage();
    page.visit;
    page.ensure;
    cy.fixture('User').as('user');
  });

  it('should successfully submit', () => {
    cy.route('POST', '/login', 'fx:response/auth/loginSuccess').as('getTokenPayload');
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    cy.get('@user').then((userJson: any) => {
      page.username.type(userJson.username);
      page.password.type(userJson.password);
      page.submit.click();
      cy.wait('@getTokenPayload');
      const feedPage = new FeedPage();
      feedPage.ensure;
    });
  });

  it('should handle expired token', () => {
    cy.login();
    const feedPage = new FeedPage();
    cy.route({
      method: 'GET',
      url: '/api/themes',
      status: 401,
      response: 'fx:response/themes/expiredToken',
    }).as('getThemes');
    feedPage.visit;
    cy.wait('@getThemes');
    page.ensure;
  });
});
