import { FeedPage } from '../../pages';

context('When user is on feed page', () => {
  let page: FeedPage;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  beforeEach(() => {
    page = new FeedPage();
    page.visit;
    page.ensure;
    cy.fixture('User').as('user');
  });
});
