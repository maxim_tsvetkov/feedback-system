export class FeedPage {
  get visit() {
    return cy.visit('/feed');
  }

  get ensure() {
    return cy.get("[data-test-id='feedPage']");
  }

  get username() {
    return cy.get('input[name=username');
  }

  get password() {
    return cy.get('input[name=password');
  }

  get submit() {
    return cy.get('button[type=submit');
  }
}
